import { Sequelize, DataTypes } from "sequelize";

const sequelize = new Sequelize('logistics', 'root', '123', {
    host: 'localhost',
    dialect: 'mysql', 
});

const Customer = sequelize.define('Customer', {
    id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    position: {
        type: DataTypes.STRING,
        allowNull: false
    },
    office: {
        type: DataTypes.STRING,
        allowNull: false
    },
    age: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    salary: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
});

sequelize.sync()
    .then(() => {
        console.log('Table created successfully.');
    })
    .catch((error) => {
        console.error('Error creating table:', error);
    });

export default Customer;
