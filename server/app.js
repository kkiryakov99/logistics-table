import express from "express";
import mysql from "mysql";
import cors from "cors";
import { createCustomer, getCustomers } from "./controllers/customer.js";

const app = express();

export const db = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "123",
    database: "logistics"
})
app.use(cors());
app.use(express.json());

app.get(`/`, getCustomers);
app.post('/', createCustomer);

app.listen(8801, () => {
    console.log(`Connected to server`)
})