import Customer from "../models/customer.js";
import { db } from "../app.js";
export const createCustomer = async (req, res) => {
    try {
        const { name, position, office, age, salary } = req.body;
        const result = await Customer.create({ name: name, position: position, office: office, age: age, salary: salary });
        return res.status(200).json(result);
    } catch (err) {
        return res.status(500).json(err);
    }
}

export const getCustomers = (req, res) => {
    db.query('SELECT * FROM logistics.customers', (err, results) => {
        if (err) {
            throw new Error("unable to retrieve customers");
        }
        return res.status(200).json(results);
    });
}