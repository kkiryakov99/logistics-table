export const addCustomer = async (customerData) => {
    try {
        const res = await fetch('http://localhost:8801/', {
            method: 'POST',
            body: JSON.stringify(customerData),
            headers: {
                'Content-Type': 'application/json'
            }
        });
        return res.json();
    } catch (err) {
        console.log(err.message);
    }
};

export const addRow = (dataTable, data) => {
    dataTable.row.add({
        "name": data.name,
        "position": data.position,
        "office": data.office,
        "age": data.age,
        "createdAt": data.createdAt,
        "salary": data.salary
    }).draw();
}

export const clearCurrentData = (customerData, inputs) => {
    customerData.name = "";
    customerData.position = "";
    customerData.office = "";
    customerData.age = "";
    customerData.salary = "";
    inputs.forEach(input => input.value = "");
}