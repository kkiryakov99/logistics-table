import { addCustomer, addRow, clearCurrentData } from "./helpers.js";

(async () => {
    const body = $('.container');
    body.append(`
<table id="example" class="table table-dark table-striped"">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Position</th>
                    <th>Office</th>
                    <th>Age</th>
                    <th>createdAt</th>
                    <th>Salary</th>
                </tr>
            </thead>
            <tbody class="table-body">
               
            </tfoot>
        </table>`
    );
    body.append(`<div class="input-group d-flex my-3 justify-content-between w-100">
<input type="text" name="name" class="form-control"  style="width: 18%" placeholder="Name">
<input type="text" name="position" class="form-control" style="width: 18%" placeholder="Position">
<input type="text" name="office" class="form-control" style="width: 18%" placeholder="Office">
<input type="number" name="age" class="form-control" style="width: 18%" placeholder="Age">
<input type="number" name="salary" class="form-control" style="width: 18%"  placeholder="Salary">
</div>`);

    body.append(`<button id="create-customer" type="button" class="btn btn-dark btn-lg btn-block w-100 mt-2 ">Create Customer</button>`);
    const result = await fetch('http://localhost:8801/');
    const customers = await result.json();
    const dataTable = $('#example').DataTable({
        data: customers,
        columns: [
            { data: 'name' },
            { data: 'position' },
            { data: 'office' },
            { data: 'age' },
            { data: 'createdAt' },
            { data: 'salary' }
        ]
    });

    const customerData = { name: "", position: "", office: "", age: "", salary: "" };

    $('.input-group').on('input', 'input', () => {
        // adds event listeners to the inputs
        customerData.name = $('input[name="name"]').val();
        customerData.position = $('input[name="position"]').val();
        customerData.office = $('input[name="office"]').val();
        customerData.age = $('input[name="age"]').val();
        customerData.salary = $('input[name="salary"]').val();
    });

    $('#create-customer').on("click", async () => {
        // input validation
        if (Object.values(customerData).includes("")) {
            return alert("Please fill in the fields!");
        }
        const data = await addCustomer(customerData);
        addRow(dataTable, data);
        // clear inputs fields

        const inputs = Object.values($('.input-group input')).slice(0, 5);
        clearCurrentData(customerData, inputs);
    });

    $('.table-body tr').css('color', '#808080');
    body.css('background-color', "#2a2a2a");
    body.css('color', "#fff");
    $('.form-control').css('background-color', '#212529');
    $('.form-control').css('color', '#FFF');
})();

